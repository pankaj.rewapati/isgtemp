<?php
 // created: 2021-12-28 09:36:54
$layout_defs["cstm_state"]["subpanel_setup"]['cstm_state_cstm_city'] = array (
  'order' => 100,
  'module' => 'cstm_city',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CSTM_STATE_CSTM_CITY_FROM_CSTM_CITY_TITLE',
  'get_subpanel_data' => 'cstm_state_cstm_city',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
