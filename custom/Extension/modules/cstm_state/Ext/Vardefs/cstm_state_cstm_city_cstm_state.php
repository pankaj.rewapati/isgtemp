<?php
// created: 2021-12-28 09:36:54
$dictionary["cstm_state"]["fields"]["cstm_state_cstm_city"] = array (
  'name' => 'cstm_state_cstm_city',
  'type' => 'link',
  'relationship' => 'cstm_state_cstm_city',
  'source' => 'non-db',
  'module' => 'cstm_city',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_CSTM_STATE_CSTM_CITY_FROM_CSTM_CITY_TITLE',
);
