<?php
// created: 2021-12-28 09:40:24
$dictionary["cstm_state"]["fields"]["cstm_country_cstm_state"] = array (
  'name' => 'cstm_country_cstm_state',
  'type' => 'link',
  'relationship' => 'cstm_country_cstm_state',
  'source' => 'non-db',
  'module' => 'cstm_country',
  'bean_name' => 'cstm_country',
  'vname' => 'LBL_CSTM_COUNTRY_CSTM_STATE_FROM_CSTM_COUNTRY_TITLE',
  'id_name' => 'cstm_country_cstm_statecstm_country_ida',
);
$dictionary["cstm_state"]["fields"]["cstm_country_cstm_state_name"] = array (
  'name' => 'cstm_country_cstm_state_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CSTM_COUNTRY_CSTM_STATE_FROM_CSTM_COUNTRY_TITLE',
  'save' => true,
  'id_name' => 'cstm_country_cstm_statecstm_country_ida',
  'link' => 'cstm_country_cstm_state',
  'table' => 'cstm_country',
  'module' => 'cstm_country',
  'rname' => 'name',
);
$dictionary["cstm_state"]["fields"]["cstm_country_cstm_statecstm_country_ida"] = array (
  'name' => 'cstm_country_cstm_statecstm_country_ida',
  'type' => 'link',
  'relationship' => 'cstm_country_cstm_state',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CSTM_COUNTRY_CSTM_STATE_FROM_CSTM_STATE_TITLE',
);
