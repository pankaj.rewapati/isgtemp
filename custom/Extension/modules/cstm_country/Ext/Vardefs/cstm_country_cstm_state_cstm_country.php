<?php
// created: 2021-12-28 09:40:24
$dictionary["cstm_country"]["fields"]["cstm_country_cstm_state"] = array (
  'name' => 'cstm_country_cstm_state',
  'type' => 'link',
  'relationship' => 'cstm_country_cstm_state',
  'source' => 'non-db',
  'module' => 'cstm_state',
  'bean_name' => 'cstm_state',
  'side' => 'right',
  'vname' => 'LBL_CSTM_COUNTRY_CSTM_STATE_FROM_CSTM_STATE_TITLE',
);
