<?php
// created: 2021-12-28 09:36:54
$dictionary["cstm_state_cstm_city"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'cstm_state_cstm_city' => 
    array (
      'lhs_module' => 'cstm_state',
      'lhs_table' => 'cstm_state',
      'lhs_key' => 'id',
      'rhs_module' => 'cstm_city',
      'rhs_table' => 'cstm_city',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cstm_state_cstm_city_c',
      'join_key_lhs' => 'cstm_state_cstm_citycstm_state_ida',
      'join_key_rhs' => 'cstm_state_cstm_citycstm_city_idb',
    ),
  ),
  'table' => 'cstm_state_cstm_city_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'cstm_state_cstm_citycstm_state_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'cstm_state_cstm_citycstm_city_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'cstm_state_cstm_cityspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'cstm_state_cstm_city_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cstm_state_cstm_citycstm_state_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'cstm_state_cstm_city_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cstm_state_cstm_citycstm_city_idb',
      ),
    ),
  ),
);