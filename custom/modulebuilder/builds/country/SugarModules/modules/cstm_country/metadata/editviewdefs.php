<?php
$module_name = 'cstm_country';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'capital',
            'label' => 'LBL_CAPITAL',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'currency_symbol',
            'label' => 'LBL_CURRENCY_SYMBOL',
          ),
          1 => 
          array (
            'name' => 'iso2',
            'label' => 'LBL_ISO2',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'numeric_code',
            'label' => 'LBL_NUMERIC_CODE',
          ),
          1 => 
          array (
            'name' => 'phonecode',
            'label' => 'LBL_PHONECODE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'currency',
            'label' => 'LBL_CURRENCY',
          ),
          1 => 'description',
        ),
        4 => 
        array (
          0 => 'assigned_user_name',
          1 => '',
        ),
      ),
    ),
  ),
);
;
?>
