<?php
// created: 2021-12-28 09:40:24
$dictionary["cstm_country_cstm_state"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'cstm_country_cstm_state' => 
    array (
      'lhs_module' => 'cstm_country',
      'lhs_table' => 'cstm_country',
      'lhs_key' => 'id',
      'rhs_module' => 'cstm_state',
      'rhs_table' => 'cstm_state',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cstm_country_cstm_state_c',
      'join_key_lhs' => 'cstm_country_cstm_statecstm_country_ida',
      'join_key_rhs' => 'cstm_country_cstm_statecstm_state_idb',
    ),
  ),
  'table' => 'cstm_country_cstm_state_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'cstm_country_cstm_statecstm_country_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'cstm_country_cstm_statecstm_state_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'cstm_country_cstm_statespk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'cstm_country_cstm_state_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cstm_country_cstm_statecstm_country_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'cstm_country_cstm_state_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cstm_country_cstm_statecstm_state_idb',
      ),
    ),
  ),
);