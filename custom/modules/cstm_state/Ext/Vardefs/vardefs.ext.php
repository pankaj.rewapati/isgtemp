<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-12-28 09:40:24
$dictionary["cstm_state"]["fields"]["cstm_country_cstm_state"] = array (
  'name' => 'cstm_country_cstm_state',
  'type' => 'link',
  'relationship' => 'cstm_country_cstm_state',
  'source' => 'non-db',
  'module' => 'cstm_country',
  'bean_name' => 'cstm_country',
  'vname' => 'LBL_CSTM_COUNTRY_CSTM_STATE_FROM_CSTM_COUNTRY_TITLE',
  'id_name' => 'cstm_country_cstm_statecstm_country_ida',
);
$dictionary["cstm_state"]["fields"]["cstm_country_cstm_state_name"] = array (
  'name' => 'cstm_country_cstm_state_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CSTM_COUNTRY_CSTM_STATE_FROM_CSTM_COUNTRY_TITLE',
  'save' => true,
  'id_name' => 'cstm_country_cstm_statecstm_country_ida',
  'link' => 'cstm_country_cstm_state',
  'table' => 'cstm_country',
  'module' => 'cstm_country',
  'rname' => 'name',
);
$dictionary["cstm_state"]["fields"]["cstm_country_cstm_statecstm_country_ida"] = array (
  'name' => 'cstm_country_cstm_statecstm_country_ida',
  'type' => 'link',
  'relationship' => 'cstm_country_cstm_state',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CSTM_COUNTRY_CSTM_STATE_FROM_CSTM_STATE_TITLE',
);


// created: 2021-12-28 09:36:54
$dictionary["cstm_state"]["fields"]["cstm_state_cstm_city"] = array (
  'name' => 'cstm_state_cstm_city',
  'type' => 'link',
  'relationship' => 'cstm_state_cstm_city',
  'source' => 'non-db',
  'module' => 'cstm_city',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_CSTM_STATE_CSTM_CITY_FROM_CSTM_CITY_TITLE',
);

?>