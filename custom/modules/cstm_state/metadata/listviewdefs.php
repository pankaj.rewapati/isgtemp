<?php
$module_name = 'cstm_state';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'CSTM_COUNTRY_CSTM_STATE_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CSTM_COUNTRY_CSTM_STATE_FROM_CSTM_COUNTRY_TITLE',
    'id' => 'CSTM_COUNTRY_CSTM_STATECSTM_COUNTRY_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'COUNTRY_CODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COUNTRY_CODE',
    'width' => '10%',
    'default' => true,
  ),
  'ISO2' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ISO2',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
);
;
?>
