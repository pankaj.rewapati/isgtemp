<?php
$module_name = 'cstm_state';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 'description',
          1 => 
          array (
            'name' => 'country_code',
            'label' => 'LBL_COUNTRY_CODE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'cstm_country_cstm_state_name',
          ),
          1 => 
          array (
            'name' => 'country_id',
            'label' => 'LBL_COUNTRY_ID',
          ),
        ),
      ),
    ),
  ),
);
;
?>
