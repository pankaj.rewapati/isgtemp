<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

error_reporting(E_ALL);
ini_set("display_errors", 1);



if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class LeadsController extends SugarController
{
    public function action_getAverageValue()
    {
        global $sugar_config;
        $temp_average = $_REQUEST['temp_average'];


        $MORE_THAN_NUMBER = isset($sugar_config['MORE_THAN_NUMBER']) ? $sugar_config['MORE_THAN_NUMBER'] : '';
        //$BETWEEN_THAN_LABEL1 = isset($sugar_config['BETWEEN_THAN_LABEL1']) ? $sugar_config['BETWEEN_THAN_LABEL1'] : '';
        //$BETWEEN_THAN_LABEL2 = isset($sugar_config['BETWEEN_THAN_LABEL2']) ? $sugar_config['BETWEEN_THAN_LABEL2'] : '';
        $LESS_THAN_LABEL = isset($sugar_config['LESS_THAN_LABEL']) ? $sugar_config['LESS_THAN_LABEL'] : '';

        if($MORE_THAN_NUMBER == ''){$MORE_THAN_NUMBER = 2.5;}
        //if($BETWEEN_THAN_LABEL1 == ''){$BETWEEN_THAN_LABEL1 = 2.2;}
        //if($BETWEEN_THAN_LABEL2 == ''){$BETWEEN_THAN_LABEL2 = 2.5;}
        if($LESS_THAN_LABEL == ''){$LESS_THAN_LABEL = 2.2;}
        
        $final_average = 0;
        if($temp_average >= $MORE_THAN_NUMBER){
            $final_average = 2; // create apportunity
        }else if($temp_average < $LESS_THAN_LABEL){
            $final_average = 0;// no proceed
        }else{
            $final_average = 1; // review
        } 
        echo $final_average; die;


    }
}
