<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-12-28 06:48:00
$dictionary['Lead']['fields']['annual_recurring_revenue_c']['inline_edit']='1';
$dictionary['Lead']['fields']['annual_recurring_revenue_c']['labelValue']='Annual Recurring Revenue';

 

 // created: 2021-12-28 06:47:22
$dictionary['Lead']['fields']['clients_decision_making_time_c']['inline_edit']='1';
$dictionary['Lead']['fields']['clients_decision_making_time_c']['labelValue']='Client\'s Decision Making Time';

 

 // created: 2021-12-28 06:48:30
$dictionary['Lead']['fields']['clients_expected_go_live_c']['inline_edit']='1';
$dictionary['Lead']['fields']['clients_expected_go_live_c']['labelValue']='Client\'s Expected Go-Live';

 

 // created: 2021-12-28 06:45:59
$dictionary['Lead']['fields']['clients_political_scenario_c']['inline_edit']='1';
$dictionary['Lead']['fields']['clients_political_scenario_c']['labelValue']='Clients Political Scenario';

 

 // created: 2021-12-28 06:47:41
$dictionary['Lead']['fields']['contract_term_c']['inline_edit']='1';
$dictionary['Lead']['fields']['contract_term_c']['labelValue']='Contract Term';

 

 // created: 2021-12-28 10:12:43
$dictionary['Lead']['fields']['department2_c']['inline_edit']='1';
$dictionary['Lead']['fields']['department2_c']['labelValue']='department2';

 

 // created: 2021-12-28 10:14:24
$dictionary['Lead']['fields']['department_other_c']['inline_edit']='1';
$dictionary['Lead']['fields']['department_other_c']['labelValue']='Department Other';

 

 // created: 2021-12-28 06:43:50
$dictionary['Lead']['fields']['existing_client_relationship_c']['inline_edit']='1';
$dictionary['Lead']['fields']['existing_client_relationship_c']['labelValue']='Existing Client Relationship';

 

 // created: 2021-12-28 06:50:09
$dictionary['Lead']['fields']['final_average_c']['inline_edit']='1';
$dictionary['Lead']['fields']['final_average_c']['labelValue']='final average';

 

 // created: 2021-12-28 06:47:00
$dictionary['Lead']['fields']['formal_decision_making_crite_c']['inline_edit']='1';
$dictionary['Lead']['fields']['formal_decision_making_crite_c']['labelValue']='Formal Decision Making Criteria';

 

 // created: 2021-12-28 06:29:05
$dictionary['Lead']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2021-12-28 06:29:05
$dictionary['Lead']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2021-12-28 06:29:05
$dictionary['Lead']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2021-12-28 06:29:05
$dictionary['Lead']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2021-12-28 06:45:28
$dictionary['Lead']['fields']['market_new_or_existing_c']['inline_edit']='1';
$dictionary['Lead']['fields']['market_new_or_existing_c']['labelValue']='Market New Or Existing';

 

 // created: 2021-12-28 06:48:54
$dictionary['Lead']['fields']['product_readiness_c']['inline_edit']='1';
$dictionary['Lead']['fields']['product_readiness_c']['labelValue']='Product Readiness';

 

 // created: 2021-12-28 06:44:59
$dictionary['Lead']['fields']['product_service_mix_c']['inline_edit']='1';
$dictionary['Lead']['fields']['product_service_mix_c']['labelValue']='Product Service Mix';

 

 // created: 2021-12-28 06:46:26
$dictionary['Lead']['fields']['stakeholders_buy_in_c']['inline_edit']='1';
$dictionary['Lead']['fields']['stakeholders_buy_in_c']['labelValue']='Stakeholders Buy In';

 

 // created: 2021-12-28 06:44:28
$dictionary['Lead']['fields']['strategic_importance_c']['inline_edit']='1';
$dictionary['Lead']['fields']['strategic_importance_c']['labelValue']='Strategic Importance';

 
?>