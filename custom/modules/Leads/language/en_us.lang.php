<?php
// created: 2021-12-28 10:14:24
$mod_strings = array (
  'LBL_EXISTING_CLIENT_RELATIONSHIP' => 'Existing Client Relationship',
  'LBL_STRATEGIC_IMPORTANCE' => 'Strategic Importance',
  'LBL_PRODUCT_SERVICE_MIX' => 'Product Service Mix',
  'LBL_MARKET_NEW_OR_EXISTING' => 'Market New Or Existing',
  'LBL_CLIENTS_POLITICAL_SCENARIO' => 'Clients Political Scenario',
  'LBL_STAKEHOLDERS_BUY_IN' => 'Stakeholders Buy In',
  'LBL_FORMAL_DECISION_MAKING_CRITE' => 'Formal Decision Making Criteria',
  'LBL_CLIENTS_DECISION_MAKING_TIME' => 'Client&#039;s Decision Making Time',
  'LBL_CONTRACT_TERM' => 'Contract Term',
  'LBL_ANNUAL_RECURRING_REVENUE' => 'Annual Recurring Revenue',
  'LBL_CLIENTS_EXPECTED_GO_LIVE' => 'Client&#039;s Expected Go-Live',
  'LBL_PRODUCT_READINESS' => 'Product Readiness',
  'LBL_FINAL_AVERAGE' => 'final average',
  'LBL_EDITVIEW_PANEL1' => 'Do we want to win?',
  'LBL_EDITVIEW_PANEL2' => 'Can we win?',
  'LBL_EDITVIEW_PANEL3' => 'How much is the deal worth ?',
  'LBL_EDITVIEW_PANEL4' => 'Can we deliver?',
  'LBL_DEPARTMENT2' => 'Department',
  'LBL_DEPARTMENT_OTHER' => 'Department Other',
);