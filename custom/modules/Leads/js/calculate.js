$(document).on('change', '#existing_client_relationship_c,#strategic_importance_c,#product_service_mix_c,#market_new_or_existing_c,#clients_political_scenario_c,#stakeholders_buy_in_c,#formal_decision_making_crite_c,#clients_decision_making_time_c,#contract_term_c,#annual_recurring_revenue_c,#clients_expected_go_live_c,#product_readiness_c', function(){
    calculateAverage();
});

function calculateAverage(){
	debugger;
    var existing_client_relationship_c = parseInt($('#existing_client_relationship_c').val());
    var strategic_importance_c = parseInt($('#strategic_importance_c').val());
    var product_service_mix_c = parseInt($('#product_service_mix_c').val());
    var market_new_or_existing_c = parseInt($('#market_new_or_existing_c').val());
    var clients_political_scenario_c = parseInt($('#clients_political_scenario_c').val());
    var stakeholders_buy_in_c = parseInt($('#stakeholders_buy_in_c').val());
    var formal_decision_making_crite_c = parseInt($('#formal_decision_making_crite_c').val());
    var clients_decision_making_time_c = parseInt($('#clients_decision_making_time_c').val());
    var contract_term_c = parseInt($('#contract_term_c').val());
    var annual_recurring_revenue_c = parseInt($('#annual_recurring_revenue_c').val());
    var clients_expected_go_live_c = parseInt($('#clients_expected_go_live_c').val());
    var product_readiness_c = parseInt($('#product_readiness_c').val());
    var temp_average = parseInt(existing_client_relationship_c+strategic_importance_c+product_service_mix_c+market_new_or_existing_c+clients_political_scenario_c+stakeholders_buy_in_c+formal_decision_making_crite_c+clients_decision_making_time_c+contract_term_c+annual_recurring_revenue_c+clients_expected_go_live_c+product_readiness_c)/12;
    temp_average = parseFloat(temp_average).toFixed(2);
    console.log('temp_average',temp_average);
    $('#temp_average_c').val(temp_average); 
    changeTaskValue(temp_average);
     
}

$(document).on('change', '#department2_c', function(){
    if($(this).val() == 'other'){
        $('#department_other_c').parent().parent().show();
    }else{
        $('#department_other_c').parent().parent().hide();
    }
});

if($('#department2_c').val() == 'other'){
    $('#department_other_c').parent().parent().show();
}else{
    $('#department_other_c').parent().parent().hide();
}

//$('#final_average_c').parent().parent().hide();
//$('#final_average_c').hide();


function changeTaskValue(temp_average){
    var id = '';
    var value = '';
    var sUrl = 'index.php?module=Leads&action=getAverageValue&temp_average='+temp_average;
    var callback = { 
      success: function(responce) {
          $('#final_average_c').val(responce.responseText);
          console.log('kkkkk',responce);
      }
    }
    
    YAHOO.util.Connect.asyncRequest("POST", sUrl, callback, null);
}