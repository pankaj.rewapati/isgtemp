<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-12-28 09:40:24
$layout_defs["cstm_country"]["subpanel_setup"]['cstm_country_cstm_state'] = array (
  'order' => 100,
  'module' => 'cstm_state',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CSTM_COUNTRY_CSTM_STATE_FROM_CSTM_STATE_TITLE',
  'get_subpanel_data' => 'cstm_country_cstm_state',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>