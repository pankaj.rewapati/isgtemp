<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-12-28 09:36:54
$dictionary["cstm_city"]["fields"]["cstm_state_cstm_city"] = array (
  'name' => 'cstm_state_cstm_city',
  'type' => 'link',
  'relationship' => 'cstm_state_cstm_city',
  'source' => 'non-db',
  'module' => 'cstm_state',
  'bean_name' => false,
  'vname' => 'LBL_CSTM_STATE_CSTM_CITY_FROM_CSTM_STATE_TITLE',
  'id_name' => 'cstm_state_cstm_citycstm_state_ida',
);
$dictionary["cstm_city"]["fields"]["cstm_state_cstm_city_name"] = array (
  'name' => 'cstm_state_cstm_city_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CSTM_STATE_CSTM_CITY_FROM_CSTM_STATE_TITLE',
  'save' => true,
  'id_name' => 'cstm_state_cstm_citycstm_state_ida',
  'link' => 'cstm_state_cstm_city',
  'table' => 'cstm_state',
  'module' => 'cstm_state',
  'rname' => 'name',
);
$dictionary["cstm_city"]["fields"]["cstm_state_cstm_citycstm_state_ida"] = array (
  'name' => 'cstm_state_cstm_citycstm_state_ida',
  'type' => 'link',
  'relationship' => 'cstm_state_cstm_city',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CSTM_STATE_CSTM_CITY_FROM_CSTM_CITY_TITLE',
);

?>