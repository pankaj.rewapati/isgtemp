

<div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
        <form id="ConfigureSettings" name="ConfigureSettings" enctype='multipart/form-data' method="POST"
      action="index.php?module=Administration&action=email_body&do=save">

    <span class='error'>{$error.main}</span>

    <table width="100%" cellpadding="0" cellspacing="1" border="0" class="actionsContainer">
        <tr>
            <td>
                {$BUTTONS}
            </td>
        </tr>
    </table>

    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="edit view" style="margin-top: 8px;">
        <tr><th align="left" scope="row" colspan="4"><h4>{$MOD.LBL_AUTHENTICATION_SETTINGS}</h4></th></tr>
        <tr>
            <td align="left"  style="padding-left: 10px;" scope="row" width="10%">{$MOD.MORE_THAN_LABEL}: </td>
            <td align="left" width="25%">
              <input class="form-check-input" type="number" step=0.01  name="MORE_THAN_NUMBER" id="MORE_THAN_NUMBER" value="{$config.MORE_THAN_NUMBER}">
            </td>
        </tr>
        <tr>
            <td align="left"  style="padding-left: 10px;" scope="row" width="10%">{$MOD.LESS_THAN_LABEL}: </td>
            <td align="left" width="25%">
              <input class="form-check-input" type="number" step=0.01  name="LESS_THAN_LABEL" id="LESS_THAN_LABEL" value="{$config.LESS_THAN_LABEL}">
            </td>
        </tr>
        
    </table>
   

    <div style="padding-top: 10px;">
        {$BUTTONS}
    </div>
    {$JAVASCRIPT}
</form> 
    </div>  
</div>
