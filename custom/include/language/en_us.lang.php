<?php
$GLOBALS['app_list_strings']['existing_client_relationship']=array (
  0 => 'Select',
  1 => 'Existing client with weak relationship',
  2 => 'Not / never been an NI client',
  3 => 'Existing client with strong relationship',
);
$GLOBALS['app_list_strings']['strategic_importance']=array (
  0 => 'Select',
  1 => 'Unknown',
  2 => 'Low',
  3 => 'Medium',
  4 => 'High / Key Account',
);
$GLOBALS['app_list_strings']['product_service_mix']=array (
  0 => 'Select',
  1 => 'Unknown / Not core product(s)',
  2 => 'Core product(s)',
  3 => 'Core plus value added products',
  4 => 'Core plus value added, plus new products',
);
$GLOBALS['app_list_strings']['market_new_or_existing']=array (
  0 => 'Select',
  1 => 'No physical presence',
  2 => 'No physical presence but existing customers in that market',
  3 => 'No physical presence but target market',
  4 => 'Physical presence in the market(s)',
);
$GLOBALS['app_list_strings']['clients_political_scenario']=array (
  0 => 'Select',
  1 => 'Unknown / Known but unstable',
  2 => 'Previous issues but stabilising',
  3 => 'Known and stable',
);
$GLOBALS['app_list_strings']['stakeholders_buy_in']=array (
  0 => 'Select',
  1 => 'Operational ',
  2 => 'Technical',
  3 => 'Decision Making',
  4 => 'Board',
);
$GLOBALS['app_list_strings']['formal_decision_making_criteria']=array (
  0 => 'Select',
  1 => 'Does not exist',
  2 => 'Exists but unknown',
  3 => 'Known but unclear',
  4 => 'Clearly defined',
);
$GLOBALS['app_list_strings']['clients_decision_making_time']=array (
  0 => 'Select',
  1 => 'Does not exist',
  2 => 'Not clearly defined',
  3 => 'Clearly defined but not event driven',
  4 => 'Compelling event driven (Bus / Tech)',
);
$GLOBALS['app_list_strings']['contract_term']=array (
  0 => 'Select',
  1 => 'Unknown',
  2 => '1 or 2 years',
  3 => '3-5 years',
  4 => 'Above 5 years 3',
);
$GLOBALS['app_list_strings']['annual_recurring_revenue']=array (
  0 => 'Select',
  1 => 'Unknown or ',
  2 => '100k-500k USD',
  3 => '500k-1m USD',
  4 => '>1M USD',
);
$GLOBALS['app_list_strings']['clients_expected_go_live']=array (
  0 => 'Select',
  1 => 'Unknown',
  2 => 'Within 6 months',
  3 => '6-12 months',
  4 => 'More than 12 months',
);
$GLOBALS['app_list_strings']['product_readiness']=array (
  0 => 'Select',
  1 => 'Not ready',
  2 => 'Modifications required',
  3 => 'Ready, but not currently in use',
  4 => 'Ready, but not currently in use',
);
$GLOBALS['app_list_strings']['department_list']=array (
  'finance' => 'Finance',
  'operations' => 'Operations',
  'it' => 'IT',
  'security' => 'Security',
  'business' => 'Business',
  'procurement' => 'Procurement',
  'compliance' => 'Compliance',
  'data_science' => 'Data Science',
  'other' => 'Other',
);